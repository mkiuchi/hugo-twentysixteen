# Hugo theme: Twenty Sixteen

Twenty Sixteen theme for Hugo. Rough port from Wordpress.

## Demo

My personal blog: [https://blog.mkiuchi.org](https://blog.mkiuchi.org)

## Installation

1. create new site with `hugo new site <yourdir>`
1. `cd <yourdir>`
1. `rm -f archetypes/default.md`
1. `git init`
1. `git submodule add https://github.com/mkiuchi/hugo-twentysixteen.git themes/hugo-twentysixteen`

## Create new blog entry

```shell
cd <yourdir>
hugo new posts/<filename>.md -t hugo-twentysixteen
```

the, edit `posts/<filename>.md` with your favorite text editor.

## General info about original Twenty Sixteen wordpress theme

- **Theme Name:**
    - Twenty Sixteen
- **Theme URI:**
    - https://ja.wordpress.org/themes/twentysixteen/ 
- **Contributors:**
    - the WordPress team
- **Version:**
    - 2.0
- **License:**
    - GPLv2 or later  
- **License URI:**
    - http://www.gnu.org/licenses/gpl-2.0.html  
- **Tags:**
    - Accessibility Ready, Blog, Custom Background, Custom Colors, Custom Header, Custom Menu, Editor Style, Featured Images, Flexible Header, Microformats, One Column, Post Formats, Right Sidebar, RTL Language Support, Sticky Post, Threaded Comments, Translation Ready, Two Columns
- **Description**
    - Twenty Sixteen is a modernized take on an ever-popular WordPress layout — the horizontal masthead with an optional right sidebar that works perfectly for blogs and websites. It has custom color options with beautiful default color schemes, a harmonious fluid grid using a mobile-first approach, and impeccable polish in every detail. Twenty Sixteen will make your WordPress look beautiful everywhere.


